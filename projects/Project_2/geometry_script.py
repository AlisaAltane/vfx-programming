import bpy
import os
import sys
import importlib

# Find out the root directory of the Blender Project
directory = os.path.dirname(bpy.data.filepath)
if not directory in sys.path:
    sys.path.append(directory)

import console_blender
importlib.reload(console_blender)
from console_blender import *

console("STARTS:")

console("Changing Group Input attributes using scripting")

# Function to set keyframe
def set_keyframe(nodes, path, frame, value):
    nodes[path] = value
    nodes.keyframe_insert(data_path=path, frame=frame)

# Catch any errors
try:
    console("Setup Parameters")
    console("Default name 'GeometryNodes' if you change Geometry Node name you need to change this parameter")
    nodes_name = 'GeometryNodes'    # BLUE_SPIKE

    console("Setup Group Input Parameters by joining them with Node")
    translation = 'Translation'
    rotation = 'Rotation'
    scale = 'Scale'
    animation_speed = 'AnimationSpeed'
    keyFrames = 'Keyframes'

    console("Global Context Object")
    object = bpy.context.object

    console("Get Geometry Nodes")
    nodes = bpy.context.object.modifiers[nodes_name]

    console("Get Group Inputs")
    inputs = nodes.node_group.inputs

    console("Get Group Input for the Named Group Input")
    translation_identifier = inputs[translation].identifier
    scale_identifier = inputs[scale].identifier
    rotation_identifier = inputs[rotation].identifier
    speed_identifier = inputs[animation_speed].identifier
    enable_keyframes_identifier = inputs[keyFrames].identifier

    translation_path = f'["{translation_identifier}"]'  # Sets the translation_# ID for the Keyframe animation
    rotation_path = f'["{rotation_identifier}"]'
    scale_path = f'["{scale_identifier}"]'

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0
    nodes[rotation_identifier][0] = 12
    nodes[rotation_identifier][1] = 27
    nodes[rotation_identifier][2] = 48
    nodes[scale_identifier][0] = 0.09
    nodes[scale_identifier][1] = 0.09
    nodes[scale_identifier][2] = 0.09
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 1, 0)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 1, 12)
    set_keyframe(nodes, scale_path, 1, 0.09)

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0.2
    nodes[rotation_identifier][0] = 12
    nodes[rotation_identifier][1] = 27
    nodes[rotation_identifier][2] = 52
    nodes[scale_identifier][0] = 0.09
    nodes[scale_identifier][1] = 0.09
    nodes[scale_identifier][2] = 0.09
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 20, 0.2)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 20, 12)
    set_keyframe(nodes, scale_path, 20, 0.09)

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0.3
    nodes[rotation_identifier][0] = 12
    nodes[rotation_identifier][1] = 27
    nodes[rotation_identifier][2] = 55
    nodes[scale_identifier][0] = 0.09
    nodes[scale_identifier][1] = 0.09
    nodes[scale_identifier][2] = 0.09
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 40, 0.3)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 40, 12)
    set_keyframe(nodes, scale_path, 40, 0.09)

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0.2
    nodes[rotation_identifier][0] = 12
    nodes[rotation_identifier][1] = 27
    nodes[rotation_identifier][2] = 52
    nodes[scale_identifier][0] = 0.09
    nodes[scale_identifier][1] = 0.09
    nodes[scale_identifier][2] = 0.09
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 60, 0.2)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 60, 12)
    set_keyframe(nodes, scale_path, 60, 0.09)

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0
    nodes[rotation_identifier][0] = 12
    nodes[rotation_identifier][1] = 27
    nodes[rotation_identifier][2] = 48
    nodes[scale_identifier][0] = 0.09
    nodes[scale_identifier][1] = 0.09
    nodes[scale_identifier][2] = 0.09
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 79, 0)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 79, 12)
    set_keyframe(nodes, scale_path, 79, 0.09)

    console("Update the Context (Flush Changes)")
    object.data.update()

except Exception as e:
    print(e)
    console(e)

# Catch any errors
try:
    console("Setup Parameters")
    console("Default name 'GeometryNodes' if you change Geometry Node name you need to change this parameter")
    nodes_name = 'GeometryNodes2'    # BLUE_SPIKE

    console("Setup Group Input Parameters by joining them with Node")
    translation = 'Translation'
    rotation = 'Rotation'
    scale = 'Scale'
    animation_speed = 'AnimationSpeed'
    keyFrames = 'Keyframes'

    console("Global Context Object")
    object = bpy.context.object

    console("Get Geometry Nodes")
    nodes = bpy.context.object.modifiers[nodes_name]

    console("Get Group Inputs")
    inputs = nodes.node_group.inputs

    console("Get Group Input for the Named Group Input")
    translation_identifier = inputs[translation].identifier
    scale_identifier = inputs[scale].identifier
    rotation_identifier = inputs[rotation].identifier
    speed_identifier = inputs[animation_speed].identifier
    enable_keyframes_identifier = inputs[keyFrames].identifier

    translation_path = f'["{translation_identifier}"]'  # Sets the translation_# ID for the Keyframe animation
    rotation_path = f'["{rotation_identifier}"]'
    scale_path = f'["{scale_identifier}"]'

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0
    nodes[rotation_identifier][0] = -22
    nodes[rotation_identifier][1] = 18
    nodes[rotation_identifier][2] = 135
    nodes[scale_identifier][0] = 1
    nodes[scale_identifier][1] = 1
    nodes[scale_identifier][2] = 1
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 1, 0)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 1, -22)
    set_keyframe(nodes, scale_path, 1, 1)

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0.1
    nodes[rotation_identifier][0] = -8
    nodes[rotation_identifier][1] = 29
    nodes[rotation_identifier][2] = 140
    nodes[scale_identifier][0] = 1
    nodes[scale_identifier][1] = 1
    nodes[scale_identifier][2] = 1
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 20, 0.1)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 20, -8)
    set_keyframe(nodes, scale_path, 20, 1)

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0.2
    nodes[rotation_identifier][0] = -8
    nodes[rotation_identifier][1] = 29
    nodes[rotation_identifier][2] = 140
    nodes[scale_identifier][0] = 1
    nodes[scale_identifier][1] = 1
    nodes[scale_identifier][2] = 1
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 40, 0.2)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 40, -8)
    set_keyframe(nodes, scale_path, 40, 1)

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0.1
    nodes[rotation_identifier][0] = -8
    nodes[rotation_identifier][1] = 29
    nodes[rotation_identifier][2] = 140
    nodes[scale_identifier][0] = 1
    nodes[scale_identifier][1] = 1
    nodes[scale_identifier][2] = 1
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 60, 0.1)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 60, -8)
    set_keyframe(nodes, scale_path, 60, 1)

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0
    nodes[rotation_identifier][0] = -22
    nodes[rotation_identifier][1] = 18
    nodes[rotation_identifier][2] = 135
    nodes[scale_identifier][0] = 1
    nodes[scale_identifier][1] = 1
    nodes[scale_identifier][2] = 1
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 79, 0)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 79, -22)
    set_keyframe(nodes, scale_path, 79, 1)

    console("Update the Context (Flush Changes)")
    object.data.update()

except Exception as e:
    print(e)
    console(e)




# Catch any errors
try:
    console("Setup Parameters")
    console("Default name 'GeometryNodes' if you change Geometry Node name you need to change this parameter")
    nodes_name = 'GeometryNodes3'    # BLUE_SPIKE

    console("Setup Group Input Parameters by joining them with Node")
    translation = 'Translation'
    rotation = 'Rotation'
    scale = 'Scale'
    animation_speed = 'AnimationSpeed'
    keyFrames = 'Keyframes'

    console("Global Context Object")
    object = bpy.context.object

    console("Get Geometry Nodes")
    nodes = bpy.context.object.modifiers[nodes_name]

    console("Get Group Inputs")
    inputs = nodes.node_group.inputs

    console("Get Group Input for the Named Group Input")
    translation_identifier = inputs[translation].identifier
    scale_identifier = inputs[scale].identifier
    rotation_identifier = inputs[rotation].identifier
    speed_identifier = inputs[animation_speed].identifier
    enable_keyframes_identifier = inputs[keyFrames].identifier

    translation_path = f'["{translation_identifier}"]'  # Sets the translation_# ID for the Keyframe animation
    rotation_path = f'["{rotation_identifier}"]'
    scale_path = f'["{scale_identifier}"]'

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0
    nodes[rotation_identifier][0] = -22
    nodes[rotation_identifier][1] = 18
    nodes[rotation_identifier][2] = 135
    nodes[scale_identifier][0] = 0.09
    nodes[scale_identifier][1] = 0.09
    nodes[scale_identifier][2] = 0.09
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 1, 0)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 1, -22)
    set_keyframe(nodes, scale_path, 1, 0.09)

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0.2
    nodes[rotation_identifier][0] = -8
    nodes[rotation_identifier][1] = 29
    nodes[rotation_identifier][2] = 140
    nodes[scale_identifier][0] = 0.09
    nodes[scale_identifier][1] = 0.09
    nodes[scale_identifier][2] = 0.09
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 20, 0.2)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 20, -8)
    set_keyframe(nodes, scale_path, 20, 0.09)

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0.3
    nodes[rotation_identifier][0] = -8
    nodes[rotation_identifier][1] = 29
    nodes[rotation_identifier][2] = 140
    nodes[scale_identifier][0] = 0.09
    nodes[scale_identifier][1] = 0.09
    nodes[scale_identifier][2] = 0.09
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 40, 0.3)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 40, -8)
    set_keyframe(nodes, scale_path, 40, 0.09)

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0.2
    nodes[rotation_identifier][0] = -8
    nodes[rotation_identifier][1] = 29
    nodes[rotation_identifier][2] = 140
    nodes[scale_identifier][0] = 0.09
    nodes[scale_identifier][1] = 0.09
    nodes[scale_identifier][2] = 0.09
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 60, 0.2)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 60, -8)
    set_keyframe(nodes, scale_path, 60, 0.09)

    nodes[translation_identifier][0] = 0
    nodes[translation_identifier][1] = 0
    nodes[translation_identifier][2] = 0
    nodes[rotation_identifier][0] = -22
    nodes[rotation_identifier][1] = 18
    nodes[rotation_identifier][2] = 135
    nodes[scale_identifier][0] = 0.09
    nodes[scale_identifier][1] = 0.09
    nodes[scale_identifier][2] = 0.09
    nodes[speed_identifier] = 0.05
    set_keyframe(nodes, translation_path, 79, 0)  # Insert a Keyframe
    set_keyframe(nodes, rotation_path, 79, -22)
    set_keyframe(nodes, scale_path, 79, 0.09)


    console("Update the Context (Flush Changes)")
    object.data.update()

except Exception as e:
    print(e)
    console(e)

console("ENDS:")
