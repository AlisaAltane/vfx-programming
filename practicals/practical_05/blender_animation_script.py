import random                       # Math Random
import bpy                          # Blender Python Module
import os                           # Operating System Module
import sys                          # System Module
import importlib                    # Importing Modules Module
from math import pi, radians        # Transformations Rotation

# Find out root directory of Blender Project
directory = os.path.dirname(bpy.data.filepath)
if not directory in sys.path:       # Check if this directory is in system Path
    sys.path.append(directory)      # Add Directory to Discoverable Path

import console_blender              # Import Custom Write to Console
importlib.reload(console_blender)   # Add custom module to the Blender Project (now that directory is know)
from console_blender import *       # After a reload import all the functions of the custom module

console("STARTS:")

console("Defining a Cube Mesh")


# Shape Vertices
vertices = [
        ( -1.0,   -1.0,   -1.0 ), # [0] Vertex 1
        ( -1.0,   +1.0,   -1.0 ), # [1] Vertex 2
        ( +1.0,   +1.0,   -1.0 ), # [2] Vertex 3
        ( +1.0,   -1.0,   -1.0 ), # [3] Vertex 4
        ( -1.0,   -1.0,   +1.0 ), # [4] Vertex 5
        ( -1.0,   +1.0,   +1.0 ), # [5] Vertex 6
        ( +1.0,   +1.0,   +1.0 ), # [6] Vertex 7
        ( +1.0,   -1.0,   +1.0 )  # [7] Vertex 8
]

# (index of vertices above)
faces = [
            (0, 1, 2, 3), # Front Face
            (4, 5, 6, 7), # Back Face
            (0, 1, 5, 4), # Left Face
            (0, 4, 7, 3), # Bottom Face
            (3, 2, 6, 7), # Right Face
            (1, 5, 6, 2)  # Top Face
        ]

edges = []




# Catch any errors
try:    
    console("Accessing Scenes")         
    result = bpy.data.scenes
    console(f"Scenes: {result}")

    for object in result:
        console(f"Scene Name: {object.collection.name}")

    console("Accessing Collections")
    result = bpy.data.collections
    console(f"Collections: {result}")

    for object in result:
        console(f"Collection Name: {object.name}")



    collection = bpy.data.collections.new('Custom Collection')                              # adding a collection
    collection.name = "Scripted Objects"

    bpy.context.scene.collection.children.link(collection)                                  # add collection to the scene

    layer_collection = bpy.context.view_layer.layer_collection.children[collection.name]    # set new collection as the active one
    bpy.context.view_layer.active_layer_collection = layer_collection

    
    x = 0
    y = 0
    
    for i in range(3):
        
        mesh_data = bpy.data.meshes.new("mesh_from_data")           # create mesh
        mesh_data.from_pydata(vertices, edges, faces)
        
        object = bpy.data.objects.new("mesh_object", mesh_data)     # object using mesh data
        bpy.context.collection.objects.link(object)                 # link object in the scene
    

        x += 5
        y += 5
        z = 1

        
        object.location = (x, y, z)  # adding objects to the collection


    console("Accessing Data-Blocks")
    console("Accessing Scene Collection")
    result = bpy.data.objects
    console(f"Objects in the Scene: {result}")

    console("Accessing Collections in a Scene")

    # Rotation in radians
    degrees = 45
    radian_rotation = degrees * pi / 180

    # Setup frame increment
    frames = 1
    frame_increment = 5
    offset = 10 # makes each object look a bit more dynamic
    frame_count = 80 # Max number of frames in scene

    # Set the frame count for the number of frames
    bpy.context.scene.frame_end = frame_count


    for result in bpy.data.collections:
        if result.name == collection.name:
            for object in result.objects:

                # Starting Location
                frames = frame_increment
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")

                # Transform Location
                frames += frame_increment
                object.location = (random.randint(-20, 0), random.randint(-20, 0), 3) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")




                frames += frame_increment
                object.rotation_euler = (0, 0, 0) # Rotation
                console(f"Rotation => x:{object.rotation_euler.x}, y:{object.rotation_euler.y}, x:{object.rotation_euler.z}")
                object.keyframe_insert(data_path="rotation_euler", frame=frames + offset, group="Rotation")

                frames += frame_increment
                object.rotation_euler = (radian_rotation, radian_rotation, radian_rotation) # Rotation
                console(f"Rotation => x:{object.rotation_euler.x}, y:{object.rotation_euler.y}, x:{object.rotation_euler.z}")
                object.keyframe_insert(data_path="rotation_euler", frame=frames + offset, group="Rotation")

                frames += frame_increment
                object.rotation_euler = (radians(90), radians(0), radians(0)) # Rotation
                console(f"Rotation => x:{object.rotation_euler.x}, y:{object.rotation_euler.y}, x:{object.rotation_euler.z}")
                object.keyframe_insert(data_path="rotation_euler", frame=frames + offset, group="Rotation")
                




                frames += frame_increment
                object.scale = (1.0, 2, 1.0) # Scale
                console(f"Scale => x:{object.scale.x}, y:{object.scale.y}, x:{object.scale.z}")
                object.keyframe_insert(data_path="scale", frame=frames + offset, group="Scale")

                frames += frame_increment
                object.scale = (1.0, 4, 1.0) # Scale
                console(f"Scale => x:{object.scale.x}, y:{object.scale.y}, x:{object.scale.z}")
                object.keyframe_insert(data_path="scale", frame=frames + offset, group="Scale")

                frames += frame_increment
                object.scale = (1.0, 5, 1.0) # Scale
                console(f"Scale => x:{object.scale.x}, y:{object.scale.y}, x:{object.scale.z}")
                object.keyframe_insert(data_path="scale", frame=frames + offset, group="Scale")

                frames += frame_increment
                object.scale = (1.0, 6, 1.0) # Scale
                console(f"Scale => x:{object.scale.x}, y:{object.scale.y}, x:{object.scale.z}")
                object.keyframe_insert(data_path="scale", frame=frames + offset, group="Scale")

                offset += 1
    

    # adding camera
    bpy.ops.object.camera_add(enter_editmode=False, align='VIEW', location=(20, 20, 5), rotation=(1.5708, 0, 2.3562))  # setting location and rotation

    # setting camera to be active camera
    camera = bpy.context.object
    bpy.context.scene.camera = camera

    # adding 'point' light
    bpy.ops.object.light_add(type='POINT', radius=1, align='WORLD', location=(0, 0, 5))
    light = bpy.context.object
    light.data.energy = 1000.0  # setting light energy



    # Save the blender file
    bpy.ops.wm.save_as_mainfile(filepath=bpy.data.filepath)

    console("ENDS:")

except Exception as e:
    console(e)