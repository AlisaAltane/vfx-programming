import bpy
import os

bpy.ops.object.select_all(action='SELECT')          # clear existing objects in the scene
bpy.ops.object.delete(use_global=False)

cube_locations_name = "cube_locations.csv"
cube_locations_name = r"C:\Users\kakat\Desktop\practical_02\cube_locations.csv"


#create a list
coordinates = [(-8,-5,1), (-4,-5,1), (0,-5,1), (4,-5,1), (8,-5,1)]
amount_of_cubes = len(coordinates)              #len counts how many objects are selected



# create cubes
for i in range(amount_of_cubes):        # in - test whether a value is inside a container, range() returns a sequence of numbers    
    x, y, z = coordinates[i]            # takes numbers from cubes coords
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))   # draws the cube
    
    
#adding more cubes & update location
coordinates = [(-6,-5,3), (-2,-5,3), (2,-5,3), (6,-5,3), (-4,-5,5), (0,-5,5), (4,-5,5), (-2,-5,7), (2,-5,7), (0,-5,9)]
amount_of_cubes = len(coordinates) 

#draw the cubes again
for i in range(amount_of_cubes):
    x, y, z = coordinates[i]  
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    

#remove cube location
coordinates = [(0,10,1)]
amount_of_cubes = len(coordinates)   

#draw the cubes scene again
for i in range(amount_of_cubes):
    x, y, z = coordinates[i]  
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    
    

#write file
try:
    print(f"Opening file {cube_locations_name} for append")  # try to open the file
                                                             # f means f-string
    cube_locations_file = open(cube_locations_name, "a")     # a - "Append" mode means you can add new information 
                                                             # to the file without erasing what's already there 

    for coords in coordinates:                               # write cube coordinates to the file
        cube_locations_file.write(f"{coords[0]},{coords[1]},{coords[2]}" + os.linesep)
                                                             # os.linesep - line separator 

    
    cube_locations_file.close()                              # close the file
    
    
except:
    print(f"There was an error opening {cube_locations_name}") # If there's an issue with opening the file, 
                                                               # it prints an error message 
                                                               # Otherwise, it moves to the next step
                                                               
else:
    print(f"Coordinates are written to the file {cube_locations_name}")

finally:
    print(f"Finally {cube_locations_name}")
    

#---------------------------------------------------------------------------------------------------------------    
    
#read file
try:
    print(f"Opening file {cube_locations_name} to read")
    test_file = open(cube_locations_name, "r")              # r - read mode
    
except:
    print(f"There was an error opening {cube_locations_name}")
    
else:
    bpy.ops.object.mode_set(mode='OBJECT')              # set the context to 'OBJECT' before adding cubes
    
    for line in test_file:                              # line is a variable that holds the current line of text being read 
                                                        # from the file
                                                        # in - test whether a value is inside a container
                                                        # loop through file
        x, y, z = map(float, line.strip().split(','))   # split the line and convert values to float
                                                        # split() - splits a string into a list
        bpy.ops.mesh.primitive_cube_add(size = 1, location = (x, y, z))
        if len(bpy.context.selected_objects) >= len(coordinates):   
            break                                       # len counts how many objects are selected in scene
            
    test_file.close()
    
finally:
    print(f"Finally {cube_locations_name}")

