import random                       # Math Random
import bpy                          # Blender Python Module
import os                           # Operating System Module
import sys                          # System Module
import importlib                    # Importing Modules Module
from math import pi, radians        # Transformations Rotation

# Find out root directory of Blender Project
directory = os.path.dirname(bpy.data.filepath)
if not directory in sys.path:       # Check if this directory is in system Path
    sys.path.append(directory)      # Add Directory to Discoverable Path

import console_blender              # Import Custom Write to Console
importlib.reload(console_blender)   # Add custom module to the Blender Project (now that directory is know)
from console_blender import *       # After a reload import all the functions of the custom module

console("STARTS:")

console("Defining a Cube Mesh")


# shape vertices
vertices = [
                ( -1.0,   -1.0,   -1.0 ), # [0] Vertex 1
                ( -1.0,   +1.0,   -1.0 ), # [1] Vertex 2
                ( +1.0,   +1.0,   -1.0 ), # [2] Vertex 3
                ( +1.0,   -1.0,   -1.0 ), # [3] Vertex 4
                ( -1.0,   -1.0,   +1.0 ), # [4] Vertex 5
                ( -1.0,   +1.0,   +1.0 ), # [5] Vertex 6
                ( +1.0,   +1.0,   +1.0 ), # [6] Vertex 7
                ( +1.0,   -1.0,   +1.0 )  # [7] Vertex 8
            ]


# edges
edges = [
            (0, 1), # {0} Edge 1
            (1, 2), # {1} Edge 2
            (2, 3), # {2} Edge 3
            (3, 0), # {3} Edge 4
            (4, 5), # {4} Edge 5
            (5, 6), # {5} Edge 6
            (6, 7), # {6} Edge 7
            (7, 4), # {7} Edge 8
            (1, 5), # {8} Edge 9
            (2, 6), # {9} Edge 10
            (3, 7), # {10} Edge 11
            (0, 4), # {11} Edge 12
        ]


# faces
faces = [
            (0, 1, 2, 3), # Front Face
            (4, 5, 6, 7), # Back Face
            (0, 1, 5, 4), # Left Face
            (0, 4, 7, 3), # Bottom Face
            (3, 2, 6, 7), # Right Face
            (1, 5, 6, 2)  # Top Face
        ]


plane_bottom_location = (-5, -8, -5) 
bpy.ops.mesh.primitive_plane_add(size=40, location=plane_bottom_location, rotation=(0, 0, 0))

bpy.ops.object.shade_smooth()                                                   # set color for the bottom plane
bottom_material = bpy.data.materials.new(name="Bottom_Plane_Material")
bottom_material.diffuse_color = (0.1, 0.1, 0.2, 1.0)                            # set color (RGB values)
bpy.context.object.data.materials.append(bottom_material)



plane_top_location = (-5, -8, 21) 
bpy.ops.mesh.primitive_plane_add(size=40, location=plane_top_location, rotation=(3.14159, 0, 0))  

bpy.ops.object.shade_smooth()                                                   # set color for the top plane
top_material = bpy.data.materials.new(name="Top_Plane_Material")
top_material.diffuse_color = (0.2, 0.8, 0.2, 1.0)                               # set color (RGB values)
bpy.context.object.data.materials.append(top_material)



plane_left_location = (-25, -8, 1) 
bpy.ops.mesh.primitive_plane_add(size=40, location=plane_left_location, rotation=(0, 3.14159/2, 0))

bpy.ops.object.shade_smooth()                                                   # set color for the left plane
left_material = bpy.data.materials.new(name="Left_Plane_Material")
left_material.diffuse_color = (0.2, 0.2, 0.8, 1.0)                              # set color (RGB values)
bpy.context.object.data.materials.append(left_material)



plane_right_location = (15, -8, 1) 
bpy.ops.mesh.primitive_plane_add(size=40, location=plane_right_location, rotation=(0, -3.14159/2, 0))  

bpy.ops.object.shade_smooth()                                                   # set color for the right plane
right_material = bpy.data.materials.new(name="Right_Plane_Material")
right_material.diffuse_color = (0.8, 0.8, 0.2, 1.0)                             # set color (RGB values)
bpy.context.object.data.materials.append(right_material)



plane_back_location = (-5, -28, 1) 
bpy.ops.mesh.primitive_plane_add(size=40, location=plane_back_location, rotation=(3.14159/2, 0, 0))

bpy.ops.object.shade_smooth()                                                   # set color for the back plane
back_material = bpy.data.materials.new(name="Back_Plane_Material")
back_material.diffuse_color = (0.2, 0.2, 0.2, 1.0)                              # set color (RGB values)
bpy.context.object.data.materials.append(back_material)



# Catch any errors
try:    
    console("Accessing Scenes")         
    result = bpy.data.scenes
    console(f"Scenes: {result}")

    for object in result:
        console(f"Scene Name: {object.collection.name}")

    console("Accessing Collections")
    result = bpy.data.collections
    console(f"Collections: {result}")

    for object in result:
        console(f"Collection Name: {object.name}")



    collection = bpy.data.collections.new('Custom Collection')                              # adding a collection
    collection.name = "Scripted Objects"

    bpy.context.scene.collection.children.link(collection)                                  # add collection to the scene

    layer_collection = bpy.context.view_layer.layer_collection.children[collection.name]    # set new collection as the active one
    bpy.context.view_layer.active_layer_collection = layer_collection


    
    x = 0
    y = 0
    
    for i in range(3):
        
        mesh_data = bpy.data.meshes.new("mesh_from_data")           # create mesh
        mesh_data.from_pydata(vertices, edges, faces)
        
        object = bpy.data.objects.new("mesh_object", mesh_data)     # object using mesh data
        bpy.context.collection.objects.link(object)                 # link object in the scene
    

        object.modifiers.new('Scripted_Skin', 'SKIN')               # show edges
        object.modifiers.new('Scripted_Subdivide', 'SUBSURF')       # subdivide vertices
        object.modifiers['Scripted_Subdivide'].levels = 5
        object.modifiers.new('Scripted_Bevel', 'BEVEL')             # subdivide vertices
        #object.modifiers['Scripted_Bevel'].width = 0.5
        object.modifiers['Scripted_Bevel'].width = 0.02             # smoothes corners & edges
        
        
        material = bpy.data.materials.new(name='Scripted_Material')
        material.diffuse_color = (2.0, 2.0, 3.0, 1) # RGBA 'Silver'
        material.metallic = 1
        object.data.materials.append(material)

        x += 5
        y += 5
        z = 1

        
        object.location = (x, y, z)  # adding objects to the collection



    console("Accessing Data-Blocks")
    console("Accessing Scene Collection")
    result = bpy.data.objects
    console(f"Objects in the Scene: {result}")

    console("Accessing Collections in a Scene")

    # Rotation in radians
    degrees = 45
    radian_rotation = degrees * pi / 180

    # Setup frame increment
    frames = 1
    frame_increment = 5
    offset = 10 # makes each object look a bit more dynamic
    frame_count = 80 # Max number of frames in scene

    # Set the frame count for the number of frames
    bpy.context.scene.frame_end = frame_count


    for result in bpy.data.collections:
        if result.name == collection.name:
            for object in result.objects:

                # Starting Location
                frames = frame_increment
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")

                # Transform Location
                frames += frame_increment
                object.location = (random.randint(-20, 0), random.randint(-20, 0), 3) # Translate
                console(f"Translation => x:{object.location.x}, y:{object.location.y}, x:{object.location.z}")
                object.keyframe_insert(data_path="location", frame=frames + offset, group="Translation")




                frames += frame_increment
                object.rotation_euler = (0, 0, 0) # Rotation
                console(f"Rotation => x:{object.rotation_euler.x}, y:{object.rotation_euler.y}, x:{object.rotation_euler.z}")
                object.keyframe_insert(data_path="rotation_euler", frame=frames + offset, group="Rotation")

                frames += frame_increment
                object.rotation_euler = (radian_rotation, radian_rotation, radian_rotation) # Rotation
                console(f"Rotation => x:{object.rotation_euler.x}, y:{object.rotation_euler.y}, x:{object.rotation_euler.z}")
                object.keyframe_insert(data_path="rotation_euler", frame=frames + offset, group="Rotation")

                frames += frame_increment
                object.rotation_euler = (radians(90), radians(0), radians(0)) # Rotation
                console(f"Rotation => x:{object.rotation_euler.x}, y:{object.rotation_euler.y}, x:{object.rotation_euler.z}")
                object.keyframe_insert(data_path="rotation_euler", frame=frames + offset, group="Rotation")
                




                frames += frame_increment
                object.scale = (1.0, 2, 1.0) # Scale
                console(f"Scale => x:{object.scale.x}, y:{object.scale.y}, x:{object.scale.z}")
                object.keyframe_insert(data_path="scale", frame=frames + offset, group="Scale")

                frames += frame_increment
                object.scale = (1.0, 4, 1.0) # Scale
                console(f"Scale => x:{object.scale.x}, y:{object.scale.y}, x:{object.scale.z}")
                object.keyframe_insert(data_path="scale", frame=frames + offset, group="Scale")

                frames += frame_increment
                object.scale = (1.0, 5, 1.0) # Scale
                console(f"Scale => x:{object.scale.x}, y:{object.scale.y}, x:{object.scale.z}")
                object.keyframe_insert(data_path="scale", frame=frames + offset, group="Scale")

                frames += frame_increment
                object.scale = (1.0, 6, 1.0) # Scale
                console(f"Scale => x:{object.scale.x}, y:{object.scale.y}, x:{object.scale.z}")
                object.keyframe_insert(data_path="scale", frame=frames + offset, group="Scale")

                offset += 1



    # add camera
    camera_data = bpy.data.cameras.new(name='Animation Camera')
    camera_object = bpy.data.objects.new('Animation Camera', camera_data)
    bpy.context.scene.collection.objects.link(camera_object)

    camera_object.location.x = -5
    camera_object.location.y = 50
    camera_object.location.z = 5

    # set the initial rotation
    camera_object.rotation_euler = (radians(90), radians(0), radians(-180))

    # set the active camera
    bpy.context.scene.camera = camera_object

    # set up animation for camera rotation
    camera_rotation_start = (radians(90), radians(0), radians(-180))
    camera_rotation_end = (radians(90), radians(90), radians(-180))
    frames_for_animation = 80

    # set up animation keyframes for camera rotation
    bpy.context.scene.frame_start = 1
    bpy.context.scene.frame_end = frames_for_animation

    # select the camera
    bpy.ops.object.select_all(action='DESELECT')
    camera_object.select_set(True)
    bpy.context.view_layer.objects.active = camera_object

    # set the starting keyframe
    camera_object.rotation_euler = camera_rotation_start
    camera_object.keyframe_insert(data_path="rotation_euler", frame=1)

    # set the ending keyframe
    camera_object.rotation_euler = camera_rotation_end
    camera_object.keyframe_insert(data_path="rotation_euler", frame=frames_for_animation)





    # adding 'point' light
    bpy.ops.object.light_add(type='POINT', radius=1, align='WORLD', location=(-5, 0, 5))
    light = bpy.context.object
    light.data.energy = 1000.0              # setting light energy



    # adding 'spot' light
    bpy.ops.object.light_add(type='SPOT', radius=1, align='WORLD', location=(-17, 6, 10), scale=(1, 1, 1), rotation=( -0.7, 0, 0))
    bpy.context.object.data.energy = 5000    # setting light energy

    bpy.context.object.data.color = (1, 0.00386531, 0.0641354)  # color of light
    bpy.context.space_data.context = 'OBJECT'



    # Save the blender file
    bpy.ops.wm.save_as_mainfile(filepath=bpy.data.filepath)

    console("ENDS:")

except Exception as e:
    console(e)