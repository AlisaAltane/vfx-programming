import bpy                          # Blender Module
import os                           # Operating System Module
import sys                          # System Module
import importlib                    # Importing Modules Module

# Find out root directory of Blender Project
directory = os.path.dirname(bpy.data.filepath)
if not directory in sys.path:       # Check if this directory is in system Path
    sys.path.append(directory)      # Add Directory to Discoverable Path

import console_blender              # Import Custom Write to Console
importlib.reload(console_blender)   # Add custom module to the Blender Project (now that directory is know)
from console_blender import *       # After a reload import all the functions of the custom module


# Animation States
STATE_IDLE = "idle"
STATE_SHOCKED = "shocked"
STATE_QUIET = "quiet"
STATE_SCARED = "scared"
STATE_SOBBING = "sobbing"
STATE_LAUGH = "laughing"

# Possible Inputs
EVENT_IDLE = "event_idle"
EVENT_SHOCKED = "event_shocked"
EVENT_QUIET = "event_quiet"
EVENT_SCARED = "event_scared"
EVENT_SOBBING = "event_sobbing"
EVENT_LAUGH = "event_laughing"

class Protagonist:
    def __init__(self):
        self.state = STATE_IDLE # initial state

    # Get current Protagonist State
    def getState(self):
        return self.state

    def action(self, event):

        if self.state == STATE_IDLE:        # Idle Transitions
            if event == EVENT_SHOCKED:
                print(f"State Transition : {self.state} => {STATE_SHOCKED}")
                self.state = STATE_SHOCKED

            elif event == EVENT_QUIET:
                print(f"State Transition : {self.state} => {STATE_QUIET}")
                self.state = STATE_QUIET

            elif event == EVENT_SCARED:
                print(f"State Transition : {self.state} => {STATE_SCARED}")
                self.state = STATE_SCARED

            elif event == EVENT_SOBBING:
                print(f"State Transition : {self.state} => {STATE_SOBBING}")
                self.state = STATE_SOBBING

            elif event == EVENT_LAUGH:
                print(f"State Transition : {self.state} => {STATE_LAUGH}")
                self.state = STATE_LAUGH

            else:
                print(f"State unchanged : {self.state}")


        elif self.state == STATE_SHOCKED:
            if event == EVENT_IDLE:
                print(f"State Transition : {self.state} => {STATE_IDLE}")
                self.state = STATE_IDLE
            else:
                print(f"State unchanged : {self.state}")

        elif self.state == STATE_QUIET:
            if event == EVENT_IDLE:
                print(f"State Transition : {self.state} => {STATE_IDLE}")
                self.state = STATE_IDLE
            else:
                print(f"State unchanged : {self.state}")

        elif self.state == STATE_SCARED:
            if event == EVENT_IDLE:
                print(f"State Transition : {self.state} => {STATE_IDLE}")
                self.state = STATE_IDLE
            else:
                print(f"State unchanged : {self.state}")
            
        elif self.state == STATE_SOBBING:
            if event == EVENT_IDLE:
                print(f"State Transition : {self.state} => {STATE_IDLE}")
                self.state = STATE_IDLE
            else:
                print(f"State unchanged : {self.state}")
        
        elif self.state == STATE_LAUGH:
            if event == EVENT_IDLE:
                print(f"State Transition : {self.state} => {STATE_IDLE}")
                self.state = STATE_IDLE
            else:
                print(f"State unchanged : {self.state}")