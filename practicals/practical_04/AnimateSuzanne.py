import bpy                          # Blender Module
import os                           # Operating System Module
import sys                          # System Module
import importlib                    # Importing Modules Module

# Find out root directory of Blender Project
directory = os.path.dirname(bpy.data.filepath)
if not directory in sys.path:       # Check if this directory is in system Path
    sys.path.append(directory)      # Add Directory to Discoverable Path

import console_blender              # Import Custom Write to Console
importlib.reload(console_blender)   # Add custom module to the Blender Project (now that directory is know)
from console_blender import *       # After a reload import all the functions of the custom module

import Protagonist              # Import Protagonist Console
importlib.reload(Protagonist)   # Add Protagonist to the Blender Project (now that directory is know)
from Protagonist import *       # After a reload import all the functions of the Protagonist

# Reset the value of all ShapeKeys to 0
def resetShapeKeyValues(object, keyframe):
    # Reset Shape Key Values
    for i in object.data.shape_keys.key_blocks:
        i.value = 0
        object.data.shape_keys.key_blocks[i.name].keyframe_insert("value",frame=keyframe)


def insertShapeKeyFrame(object, keyframe, key, value):
    # Reset Shape Key Values
    resetShapeKeyValues(object, keyframe)
    # Set Shape Key Value
    object.data.shape_keys.key_blocks[key].value = 1
    # Insert Keyframe
    object.data.shape_keys.key_blocks[key].keyframe_insert("value",frame=keyframe)


# Create the Protagonist
protagonist = Protagonist()

# Get the Mesh Suzanne
object = bpy.data.objects['Suzanne.001']

# Set Keyframe
keyframe = 1
# Set Protagonist State
protagonist.action(EVENT_IDLE)
# Insert Keyframe
insertShapeKeyFrame(object, keyframe, protagonist.getState(), 1)

# Set Keyframe
keyframe = 10
# Set Protagonist State
protagonist.action(EVENT_SHOCKED)
# Insert Keyframe
insertShapeKeyFrame(object, keyframe, protagonist.getState(), 1)

# Set Keyframe
keyframe = 20
# Set Protagonist State
protagonist.action(EVENT_IDLE)
# Insert Keyframe
insertShapeKeyFrame(object, keyframe, protagonist.getState(), 1)

# Set Keyframe
keyframe = 30
# Set Protagonist State
protagonist.action(EVENT_QUIET)
# Insert Keyframe
insertShapeKeyFrame(object, keyframe, protagonist.getState(), 1)

# Set Keyframe
keyframe = 40
# Set Protagonist State
protagonist.action(EVENT_IDLE)
# Insert Keyframe
insertShapeKeyFrame(object, keyframe, protagonist.getState(), 1)

# Set Keyframe
keyframe = 50
# Set Protagonist State
protagonist.action(EVENT_SCARED)
# Insert Keyframe
insertShapeKeyFrame(object, keyframe, protagonist.getState(), 1)

# Set Keyframe
keyframe = 60
# Set Protagonist State
protagonist.action(EVENT_IDLE)
# Insert Keyframe
insertShapeKeyFrame(object, keyframe, protagonist.getState(), 1)

# Set Keyframe
keyframe = 70
# Set Protagonist State
protagonist.action(EVENT_SOBBING)
# Insert Keyframe
insertShapeKeyFrame(object, keyframe, protagonist.getState(), 1)

# Set Keyframe
keyframe = 80
# Set Protagonist State
protagonist.action(EVENT_IDLE)
# Insert Keyframe
insertShapeKeyFrame(object, keyframe, protagonist.getState(), 1)

# Set Keyframe
keyframe = 90
# Set Protagonist State
protagonist.action(EVENT_LAUGH)
# Insert Keyframe
insertShapeKeyFrame(object, keyframe, protagonist.getState(), 1)

# Set Keyframe
keyframe = 99
# Set Protagonist State
protagonist.action(EVENT_IDLE)
# Insert Keyframe
insertShapeKeyFrame(object, keyframe, protagonist.getState(), 1) 


console("ENDS:")

