import bpy
import os

bpy.ops.object.select_all(action='SELECT')      # clear existing objects in the scene
bpy.ops.object.delete(use_global=False)


class Coordinate:                               # creating a class for coordinates
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z



def create_collection():                        # method to create coordinates
                                                # creating a heart pixel shape & a list of objects
    coordinates = [Coordinate(0, 0, 1), Coordinate(-2, 0, 3),Coordinate(0, 0, 3),Coordinate(2, 0, 3),
                    Coordinate(-4, 0, 5), Coordinate(-2, 0, 5), Coordinate(0, 0, 5), Coordinate(2, 0, 5), Coordinate(4, 0, 5),
                    Coordinate(-6, 0, 7), Coordinate(-4, 0, 7), Coordinate(-2, 0, 7), Coordinate(0, 0, 7), Coordinate(2, 0, 7), Coordinate(4, 0, 7), Coordinate(6, 0, 7),
                    Coordinate(-8, 0, 9), Coordinate(-6, 0, 9),Coordinate(-4, 0, 9), Coordinate(-2, 0, 9), Coordinate(0, 0, 9), Coordinate(2, 0, 9), Coordinate(4, 0, 9), Coordinate(6, 0, 9), Coordinate(8, 0, 9),
                    Coordinate(-8, 0, 11), Coordinate(-6, 0, 11),Coordinate(-4, 0, 11), Coordinate(-2, 0, 11), Coordinate(0, 0, 11), Coordinate(2, 0, 11), Coordinate(4, 0, 11), Coordinate(6, 0, 11), Coordinate(8, 0, 11),
                    Coordinate(-8, 0, 13), Coordinate(-6, 0, 13),Coordinate(-4, 0, 13), Coordinate(-2, 0, 13), Coordinate(2, 0, 13), Coordinate(4, 0, 13), Coordinate(6, 0, 13), Coordinate(8, 0, 13),
                    Coordinate(-6, 0, 15),Coordinate(-4, 0, 15), Coordinate(4, 0, 15), Coordinate(6, 0, 15)]
    return coordinates


collection = create_collection()                # calling method for coordinates location



for coord in collection:                        # loop to draw the cubes
    
    bpy.ops.mesh.primitive_cube_add(location=(coord.x, coord.y, coord.z))